from django.shortcuts import render, redirect
from django.http import HttpResponseRedirect
from .forms import Input_RS, Input_Tes
from .models import RumahSakit, PendaftarRapid, PendaftarSwab
from django.http import JsonResponse

response = {}

def savers(request):
    form = Input_RS()
    if request.is_ajax():
        form = Input_RS(request.POST)
        if form.is_valid():
            obj = RumahSakit.objects.create(nama=request.POST['nama'], 
                provinsi=request.POST['provinsi'], kabkota=request.POST['kabkota'],
                alamat=request.POST['alamat'], telepon=request.POST['telepon'])
            rs = {'id':obj.id, 'nama':obj.nama, 'provinsi': obj.provinsi, 'kabkota':
            obj.kabkota, 'alamat':obj.alamat, 'telepon':obj.telepon}
            jumlahrs = RumahSakit.objects.count()
            data = {'rs':rs, 'jumlah':jumlahrs}
            return JsonResponse(data)
    else:
        rsrujukans = RumahSakit.objects.all()
        jumlahrs = RumahSakit.objects.count()
        response = {'input_rs' : Input_RS, 'rsrujukans':rsrujukans, 'jumlahrs':jumlahrs}
        return render(request, 'daftarrs.html', response)

def detail(request, id):
    rsrujukan = RumahSakit.objects.get(id=id)
    response = {'rsrujukan' : rsrujukan}
    return render(request, "detailrs.html", response)

def hapus(request, id):
    if request.method == "POST":
        rs = RumahSakit.objects.get(pk=id)
        nama = rs.nama
        rs.delete()
        jumlahrs = RumahSakit.objects.count()
        return JsonResponse({'jumlah' : jumlahrs, 'nama' : nama})

def ubah(request, id):
    rsrujukan = RumahSakit.objects.get(id=id)
    if request.method == 'GET':
        form = Input_RS(instance=rsrujukan)
    if request.method == 'POST':
        form = Input_RS(request.POST, instance=rsrujukan)
        if form.is_valid:
            form.save()
            return HttpResponseRedirect('/daftar-rs/')
    response = {'input_rs' : form, 'rsrujukan':rsrujukan}
    return render(request, 'ubahrs.html', response)
    
def tescovid(request):
    response = {'input_tes' : Input_Tes}
    return render(request, "tescovid.html", response)

def savependaftar(request):
    form = Input_Tes(request.POST or None)
    if (form.is_valid and request.method == 'POST'):
        if (request.POST['tes'] == '1'):
            pendaftar = PendaftarRapid.objects.create(nama=request.POST['nama'], 
                no_id=request.POST['no_id'], telepon=request.POST['telepon'],
                alamat=request.POST['alamat'], tempat=request.POST['tempat'])
        else:
            pendaftar = PendaftarSwab.objects.create(nama=request.POST['nama'], 
                no_id=request.POST['no_id'], telepon=request.POST['telepon'],
                alamat=request.POST['alamat'], tempat=request.POST['tempat'])
        response = {'id' : pendaftar.id}
        return render(request, "tikettes.html", response)
    else:
        return HttpResponseRedirect('/daftar-rs/tes-covid/')
        