from django.test import TestCase, Client
from django.urls import resolve, reverse
from .views import savers, detail, hapus, ubah, tescovid, savependaftar
from .models import RumahSakit, PendaftarRapid, PendaftarSwab
from django.contrib.auth.models import User
from .admin import RumahSakit, PendaftarRapid, PendaftarSwab
from .forms import Input_RS, Input_Tes

class TestListRS(TestCase):
    #Test Model
    def setUp(self):
        self.client = Client()
        self.rumah_sakit = RumahSakit.objects.create(
            nama="RSUD Pandemi Kelar",
            provinsi="Jawa Barat",
            kabkota="Depok",
            alamat="Jl. Sehat No. 18",
            telepon="081234567890"
        )
        self.test_rs_delete_url = reverse("list_rs:hapus",args=[self.rumah_sakit.id])
        self.pendaftar_rapid = PendaftarRapid.objects.create(
            nama="Antiqo Vidna Intin",
            no_id="26347191037",
            telepon="081265334567",
            alamat="Perum. Nopandemi Blok N-0 Depoks",
            tempat="Klinik Haru Biru"
        )
        self.pendaftar_swab = PendaftarSwab.objects.create(
            nama="Semo Ganega Tiff",
            no_id="073825239",
            telepon="081265334566",
            alamat="Jl. Sumber Sehat No. 123 Depoks",
            tempat="RSUD Bintang Putih"
        )
        self.test_user = User.objects.create_user(username="manusia")
        self.test_user.set_password("orang1209")
        self.test_user.save()

    def test_model_rumah_sakit_ada(self):
        hitung_rumah_sakit = RumahSakit.objects.all().count()
        self.assertEquals(hitung_rumah_sakit, 1)
    def test_model_pendaftarrapid_ada(self):
        hitung_pendaftar_rapid = PendaftarRapid.objects.all().count()
        self.assertEquals(hitung_pendaftar_rapid, 1)
    def test_model_pendaftarswab_ada(self):
        hitung_pendaftar_swab = PendaftarSwab.objects.all().count()
        self.assertEquals(hitung_pendaftar_swab, 1)
    def test_str_rumah_sakit(self):
        self.assertEquals(str(self.rumah_sakit), self.rumah_sakit.nama)

    #Test URL
    def test_url_list_rs_sebelum_login(self):
        response = self.client.get('/daftar-rs/')
        self.assertFalse(response.context['user'].is_authenticated)
        self.assertEquals(response.status_code, 200)
        html_kembalian = response.content.decode('utf8')
        self.assertIn("Ingin menambahkan rumah sakit rujukan pada daftar di bawah?", html_kembalian)
        self.assertIn("Login", html_kembalian)
        self.assertIn("Belum punya akun?", html_kembalian)
        self.assertIn("Buat akun baru", html_kembalian)
        self.assertNotIn("Daftarkan Rumah Sakit Rujukan Anda.", html_kembalian)
        self.assertNotIn("Submit", html_kembalian)
        self.assertNotIn("Ubah", html_kembalian)
        self.assertNotIn("Hapus", html_kembalian)
    def test_url_list_rs_sesudah_login(self):
        self.client.login(username="manusia", password="orang1209")
        response = self.client.get('/daftar-rs/')
        self.assertTrue(response.context['user'].is_authenticated)
        self.assertEquals(response.status_code, 200)
        html_kembalian = response.content.decode('utf8')
        self.assertIn("Daftarkan Rumah Sakit Rujukan Anda.", html_kembalian)
        self.assertIn("Submit", html_kembalian)
        self.assertIn("Ubah", html_kembalian)
        self.assertIn("Hapus", html_kembalian)
    def test_url_hapus_ada(self):
        response = self.client.post('/daftar-rs/hapus/1')
        self.assertEquals(response.status_code, 200)
    def test_url_detail_ada(self):
        response = self.client.get('/daftar-rs/detail/1')
        self.assertEquals(response.status_code, 200)
    def test_url_ubah_ada(self):
        response = self.client.get('/daftar-rs/ubah/1')
        self.assertEquals(response.status_code, 200)
    def test_url_tes_covid_sebelum_login(self):
        response = self.client.get('/daftar-rs/tes-covid/')
        self.assertFalse(response.context['user'].is_authenticated)
        self.assertEquals(response.status_code, 200)
        html_kembalian = response.content.decode('utf8')
        self.assertIn("Ingin mendaftar rapid test atau swab test?", html_kembalian)
        self.assertIn("Login", html_kembalian)
        self.assertIn("Belum punya akun?", html_kembalian)
        self.assertIn("Buat akun baru", html_kembalian)
        self.assertNotIn("Rapid / Swab:", html_kembalian)
        self.assertNotIn("Submit", html_kembalian)
    def test_url_tes_covid_sesudah_login(self):
        self.client.login(username="manusia", password="orang1209")
        response = self.client.get('/daftar-rs/tes-covid/')
        self.assertTrue(response.context['user'].is_authenticated)
        self.assertEquals(response.status_code, 200)
        html_kembalian = response.content.decode('utf8')
        self.assertIn("Rapid / Swab:", html_kembalian)
        self.assertIn("Submit", html_kembalian)
    def test_url_tiket_tes_ada(self):
        response = self.client.get('/daftar-rs/tes-covid/tiket')
        self.assertEquals(response.status_code, 302)
    
    def test_url_memanggil_savers(self):
        found = resolve('/daftar-rs/')
        self.assertEquals(found.func, savers)
    def test_url_memanggil_detail(self):
        found = resolve('/daftar-rs/detail/1')
        self.assertEquals(found.func, detail)
    def test_url_memanggil_ubah(self):
        found = resolve('/daftar-rs/ubah/1')
        self.assertEquals(found.func, ubah)
    def test_url_memanggil_hapus(self):
        found = resolve('/daftar-rs/hapus/1')
        self.assertEquals(found.func, hapus)
    def test_url_memanggil_tescovid(self):
        found = resolve('/daftar-rs/tes-covid/')
        self.assertEquals(found.func, tescovid)
    def test_url_memanggil_savependaftar(self):
        found = resolve('/daftar-rs/tes-covid/tiket')
        self.assertEquals(found.func, savependaftar)
    
    #Test Views
    def test_rumah_sakit_telah_disimpan_dan_ditampilkan(self):
        self.client.login(username="manusia", password="orang1209")
        response = self.client.post('/daftar-rs/', {
            'nama':'RSUD Pelangi',
            'provinsi':'Jawa Selatan',
            'kabkota':'Semanggi',
            'alamat':'Jl. Geding Brua No. 11',
            'telepon' : '0332-638291'
            }, **{'HTTP_X_REQUESTED_WITH': 'XMLHttpRequest'})
        result = self.client.get('/daftar-rs/')
        self.assertTrue(result.context['user'].is_authenticated)
        html_kembalian = result.content.decode('utf8')
        self.assertIn("RSUD Pelangi", html_kembalian)
    def test_detail_rumah_sakit_ditampilkan(self):
        response = self.client.get(reverse('list_rs:detail', kwargs={'id': self.rumah_sakit.id}))
        self.assertEqual(response.status_code, 200)
        html_kembalian = response.content.decode('utf8')
        self.assertIn("Jawa Barat", html_kembalian)
    def test_fungsi_hapus_bekerja(self): 
        response = self.client.post(self.test_rs_delete_url, follow=True)
        result = self.client.get('/daftar-rs/')
        html_kembalian = result.content.decode('utf8')
        self.assertNotIn("RSUD Pandemi Kelar", html_kembalian)
        self.assertIn("Belum ada rumah sakit rujukan yang terdaftar.", html_kembalian)
    def test_fungsi_ubah_bekerja(self):
        self.client.login(username="manusia", password="orang1209")
        response = self.client.post('/daftar-rs/ubah/1', {
            'nama':'RSUD Pelangi',
            'provinsi':'Jawa Selatan',
            'kabkota':'Semanggi',
            'alamat':'Jl. Geding Brua No. 11',
            'telepon' : '0332-638291'
            }, follow=True)
        self.assertTrue(response.context['user'].is_authenticated)
        html_kembalian = response.content.decode('utf8')
        self.assertNotIn("RSUD Pandemi Kelar", html_kembalian)
    def test_pendaftar_rapid_telah_disimpan_dan_no_antrian_ditampilkan(self):
        self.client.login(username="manusia", password="orang1209")
        response = self.client.post('/daftar-rs/tes-covid/tiket', {
            'nama':'Uncle Roger',
            'no_id':'3542630283',
            'telepon':'09876545612',
            'alamat':'Jl. Haiyyaa No. 75',
            'tempat' : 'RS Zhongli',
            'tes' : '1'
            })
        self.assertTrue(response.context['user'].is_authenticated)
        html_kembalian = response.content.decode('utf8')
        self.assertIn("Nomor Antrian Anda:", html_kembalian)
        self.assertIn("1", html_kembalian)
    def test_pendaftar_swab_telah_disimpan_dan_no_antrian_ditampilkan(self):
        self.client.login(username="manusia", password="orang1209")
        response = self.client.post('/daftar-rs/tes-covid/tiket', {
            'nama':'Uncle Roger',
            'no_id':'3542630283',
            'telepon':'09876545612',
            'alamat':'Jl. Haiyyaa No. 75',
            'tempat' : 'RS Zhongli',
            'tes' : '2'
            })
        self.assertTrue(response.context['user'].is_authenticated)
        html_kembalian = response.content.decode('utf8')
        self.assertIn("Nomor Antrian Anda:", html_kembalian)
        self.assertIn("2", html_kembalian)
    