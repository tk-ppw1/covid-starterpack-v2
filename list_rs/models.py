from django.db import models

class RumahSakit(models.Model):
    nama = models.CharField(max_length=200)
    provinsi = models.CharField(max_length=200)
    kabkota = models.CharField(max_length=200)
    alamat = models.TextField(blank=False, null=False)
    telepon = models.CharField(max_length=200)

    def __str__(self):
        return self.nama

class PendaftarRapid(models.Model):
    nama = models.CharField(max_length=200)
    no_id = models.CharField(max_length=200)
    telepon = models.CharField(max_length=200)
    alamat = models.TextField(blank=False, null=False)
    tempat = models.CharField(max_length=200)

class PendaftarSwab(models.Model):
    nama = models.CharField(max_length=200)
    no_id = models.CharField(max_length=200)
    telepon = models.CharField(max_length=200)
    alamat = models.TextField(blank=False, null=False)
    tempat = models.CharField(max_length=200)
