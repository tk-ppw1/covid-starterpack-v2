from django.contrib import admin
from .models import RumahSakit, PendaftarRapid, PendaftarSwab

admin.site.register(RumahSakit)
admin.site.register(PendaftarRapid)
admin.site.register(PendaftarSwab)