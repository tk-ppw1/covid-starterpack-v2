from django.urls import path

from . import views

app_name = 'list_rs'

urlpatterns = [
    path('', views.savers, name='savers'),
    path('detail/<str:id>', views.detail, name='detail'),
    path('hapus/<str:id>', views.hapus, name='hapus'),
    path('ubah/<str:id>', views.ubah, name='ubah'),
    path('tes-covid/', views.tescovid, name='tescovid'),
    path('tes-covid/tiket', views.savependaftar),
]