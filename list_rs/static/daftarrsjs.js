$(document).ready(function(){

    $(document).on("submit","#rs_form",function(e) {
        e.preventDefault();
        $.ajax({
            url:"",
            method:"POST",
            data: $("#rs_form").serialize(),
            dataType: "json",
            success: function(data) {
                if (data.rs) {
                    if (data.jumlah == 1) {
                        $("#list > tbody:last-child").remove();
                    }
                    var output = '<tr id="' + data.rs.id + '">' + 
                    '<td><a href="/daftar-rs/detail/' + data.rs.id + '" class="detail">' + data.rs.nama + '</a></td>' +
                    '<td>' + data.rs.provinsi + '</td>' +
                    '<td class="center"><form action="/daftar-rs/ubah/' + data.rs.id + '"><input type="submit" class="btn btn-warning" value="Ubah"></form>' + 
                    '</td><td class="center">' + "<input type='button' class='btn btn-danger btn-del' value='Hapus' id="
                    + data.rs.id + "></td></tr>"
                    $("#list > tbody:last-child").append(output);
                    $("#jumlah").html("Telah terdaftar sebanyak " + data.jumlah + " rumah sakit rujukan yang siap menangani pasien COVID-19 pada website ini." + 
                        "<br><br>Tambahkan jumlah rumah sakit rujukan COVID-19 agar pandemi ini segera berakhir.");
                    alert(data.rs.nama + ' berhasil ditambahkan ke daftar rumah sakit rujukan!');
                    $("#rs_form").trigger('reset');
                    var trelmnt = document.getElementById(data.rs.id);
                    trelmnt.scrollIntoView();
                    var tr = "#" + data.rs.id;
                    $(tr).css("background-color","#ffff99");
                    setTimeout(function () {
                        $(tr).css("background-color","");
                    }, 700);
                    $("#search").val("");
                    $("tr").show();
                }
            }
        });
    });

    $("tbody").on("click",".btn-del", function() {
        var action = confirm("Apakah Anda yakin ingin menghapus rumah sakit rujukan ini?");
        if (action != false) {
            var id = $(this).attr("id");
            var mythis = $(this);
            var csrf = $("input[name=csrfmiddlewaretoken").val();
            mydata = {'id':id, 'csrfmiddlewaretoken':csrf};
            $.ajax({
                url:"hapus/" + id,
                method: "POST",
                data:mydata,
                dataType: "json",
                success: function(data) {
                    alert(data.nama + " berhasil dihapus!");
                    mythis.parents("tr").fadeOut(300, function() {
                        $(this).remove();
                        $("#search").val("");
                        $("tr").show();
                    });
                    if (data.jumlah == 0) {
                        output = '<tr class="text-center"><td colspan="4"><p>Belum ada rumah sakit rujukan yang terdaftar.</p></td></tr>';
                        $("#list > tbody:last-child").append(output);
                    }
                    $("#jumlah").html("Telah terdaftar sebanyak " + data.jumlah + " rumah sakit rujukan yang siap menangani pasien COVID-19 pada website ini." + 
                        "<br><br>Tambahkan jumlah rumah sakit rujukan COVID-19 agar pandemi ini segera berakhir.");
                },
            });
        }
    });

    $("#search").on("input", function() {
        if ($(this).val() != "") {
            var value = $(this).val().toLowerCase();
            $("#list > tbody > tr").filter(function() {
                $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1);
            });
        } else {
            $("tr").show();
        }
    });
    
    $("form").on("change", "input", function() {
        var value = $(this).attr("value");
        if (value != "Ubah" && value != "Submit") {
            if ($(this).val() != "") {
                $(this).css("background-color", "#DDF5F4");
            } else {
                $(this).css("background-color", "#FFE9E9");
            }
        }
    });

    $("form").on("change", "textarea", function() {
        if ($(this).val() != "") {
            $(this).css("background-color", "#DDF5F4");
        } else {
            $(this).css("background-color", "#FFE9E9");
        }
    });

    $("form").on("change", "select", function() {
        $(this).css("background-color", "#DDF5F4");
    });

    $("#list").on("mouseenter", "tbody", function() {
        $(".klik").css("text-align", "center");
        $(".klik").css("position", "relative");
        $(".klik").css("bottom", "23px");
        $(".klik").fadeIn();
    });

    $("#list").on("mouseleave", "tbody", function() {
        $(".klik").fadeOut();
    });

});