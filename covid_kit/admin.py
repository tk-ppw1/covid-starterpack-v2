from django.contrib import admin
from .models import Barang, Pesanan

# Register your models here.
admin.site.register(Barang)
admin.site.register(Pesanan)