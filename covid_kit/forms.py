from django import forms
from .models import Barang, Pesanan

class BarangForm(forms.Form):
    nama = forms.CharField(widget = forms.TextInput(attrs={'style':'width:90%; border-color: white', 'id':'nama'}))
    deskripsi = forms.CharField(widget = forms.TextInput(attrs={'style':'width:90%; border-color: white', 'id':'deskripsi'}))
    foto = forms.CharField(widget = forms.TextInput(attrs={'style':'width:90%; border-color: white', 'id':'foto'}))

class PesanForm(forms.Form):
    nama_pemesan = forms.CharField(widget = forms.TextInput(attrs={'style':'width:90%; border-color: white'}))
    alamat = forms.CharField(widget = forms.TextInput(attrs={'style':'width:90%; border-color: white'}))
    no_hp = forms.CharField(widget = forms.TextInput(attrs={'style':'width:90%; border-color: white'}))
    pesanan = forms.CharField(widget = forms.TextInput(attrs={'style':'width:90%; border-color: white'}))