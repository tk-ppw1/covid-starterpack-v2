from django.db import models

# Create your models here.
class Barang(models.Model):
    nama = models.CharField(max_length=50)
    deskripsi = models.CharField(max_length=200)
    foto = models.CharField(max_length=100)

class Pesanan(models.Model):
    nama_pemesan = models.CharField(max_length=50)
    alamat = models.CharField(max_length=200)
    no_hp = models.CharField(max_length=15)
    pesanan = models.CharField(max_length=200)