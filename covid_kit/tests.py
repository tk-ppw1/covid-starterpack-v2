import json
from django.test import TestCase, Client
from django.urls import resolve, reverse
from .views import barang, daftar_barang, daftar_pesanan, berhasil, buat_barang
from .models import Barang, Pesanan
from .apps import CovidKitConfig
from django.contrib.auth.models import User

# Create your tests here.
class TestNamaApp(TestCase):
	def test_nama_app(self):
		self.assertEqual(CovidKitConfig.name, 'covid_kit')

class TestModel(TestCase):
	def test_event_model_create_barang_object(self):
		requestorang = Barang(nama="light stick", deskripsi="bagus", foto="asdfghjkl")
		requestorang.save()
		self.assertEqual(Barang.objects.all().count(), 1)
		
	def test_event_model_create_pesanan_object(self):
		pesananorang = Pesanan(nama_pemesan="evita", alamat="jalan yang benar", no_hp="081234567890", pesanan="hand sanitizer")
		pesananorang.save()
		self.assertEqual(Pesanan.objects.all().count(), 1)

class TestViews(TestCase):
	def test_event_using_template(self):
		response = Client().get('/covid_kit/')
		self.assertTemplateUsed(response, 'kit.html')
	
	def test_menggunakan_template_berhasil(self):
		response = Client().get('/covid_kit/berhasil/')
		self.assertEqual(response.status_code, 200)
	
	# def test_bisa_request_barang(self):
	# 	data = {
	# 		"nama":"kkami",
	# 		"deskripsi":"anjing",
	# 		"foto":"asdfghjkl"
	# 	}
	# 	response = Client().post('/covid_kit/daftar/', data=data)

	# 	count = Barang.objects.count()

	# 	self.assertEqual(response.status_code, 200)
	
	def test_bisa_pesan_barang(self):
		data = {
			"nama_pemesan":"haje",
			"alamat":"jalan yang benar",
			"no_hp":"081234567890",
			"pesanan":"hand sanitizer 100 pcs"
		}
		response = Client().post('/covid_kit/pesan/', data=data)

		count = Pesanan.objects.count()

		self.assertEqual(response.status_code, 302)
	
	def test_bisa_request_barang(self):
		self.barang = reverse('covid_kit:daftar')
		response = Client().post(self.barang, data={
			'nama':'kkami',
			'deskripsi':'anjing',
			'foto':'asdfghjkl'
		})
		self.assertEqual(response.status_code, 200)
	
	def test_json(self):
		dic = {
			"nama" : "kkami",
			"deskripsi" : "anjing",
			"foto" : "asdfghjkl"
		}
		response = self.client.post('/covid_kit/buat_barang/', dic)

	def test_json_form_not_valid(self):
		dic = {
			"deskripsi" : "anjing",
			"foto" : "asdfghjkl"
		}
		response = self.client.post('/covid_kit/buat_barang/', dic)

class TestUrls(TestCase):
	def test_apakah_url_covid_kit_ada(self):
		response = Client().get('/covid_kit/')
		self.assertEqual(response.status_code, 200)

	def test_apakah_url_daftar_ada(self):
		response = Client().get('/covid_kit/daftar/')
		self.assertEqual(response.status_code, 200)

	def test_apakah_url_pesan_ada(self):
		response = Client().get('/covid_kit/pesan/')
		self.assertEqual(response.status_code, 200)

	def test_apakah_berhasil_ada(self):
		found = resolve('/covid_kit/berhasil/')
		self.assertEqual(found.func, berhasil)

class TestLogin(TestCase):
    def setUp(self):
        self.client = Client()
        self.user_baru = User.objects.create_user(username='haje')
        self.user_baru.set_password('haje12345')
        self.user_baru.save()

    def test_belum_login(self):
        response = self.client.get('/covid_kit/')
        self.assertFalse(response.context['user'].is_authenticated)
    
    def test_sudah_login(self):
        self.client.login(username='haje', password='haje12345')
        response = self.client.get('/covid_kit/')
        self.assertTrue(response.context['user'].is_authenticated)
        html_response = response.content.decode('utf8')