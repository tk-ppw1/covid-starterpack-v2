from django.apps import AppConfig


class CovidKitConfig(AppConfig):
    name = 'covid_kit'
