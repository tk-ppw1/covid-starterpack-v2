from django.shortcuts import render, HttpResponseRedirect
from .models import Barang, Pesanan
from .forms import BarangForm, PesanForm
from django.http import JsonResponse, HttpResponseNotFound, Http404

# Create your views here.
def barang(request):
    return render(request, "kit.html")

def berhasil(request):
    return render(request, "berhasil.html")

def daftar_barang(request):
    form = BarangForm()
        #return HttpResponseRedirect('/covid_kit/daftar')
    
    barangList = Barang.objects.all()
    response = {
        'form':form,
        'barangs':barangList
    }
    return render(request, "form.html", response)

def buat_barang(request):
    response_data = {}
    if request.method == 'POST':
        form = BarangForm(request.POST)

        if form.is_valid():
            nama = form.cleaned_data['nama']
            deskripsi = form.cleaned_data['deskripsi']
            foto = form.cleaned_data['foto']

            barang = Barang(nama = nama, deskripsi = deskripsi, foto = foto)
            barang.save()

            response_data['nama'] = barang.nama
            response_data['deskripsi'] = barang.deskripsi
            response_data['foto'] = barang.foto

            return JsonResponse(response_data)

        raise Http404()

def daftar_pesanan(request):
    form = PesanForm()
    if request.method == 'POST':
        form = PesanForm(request.POST)

        Pesanan.objects.create(
            nama_pemesan = request.POST['nama_pemesan'],
            alamat = request.POST['alamat'],
            no_hp = request.POST['no_hp'],
            pesanan = request.POST['pesanan']
        )

        return HttpResponseRedirect('/covid_kit/berhasil')
    
    response = {
        'form':form
    }
    return render(request, "formpesan.html", response)