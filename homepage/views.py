from django.shortcuts import render
from berita.models import Berita
from list_rs.models import RumahSakit
# Create your views here.
def homepage(request):
    context = {
        "listBerita" : Berita.objects.all().order_by("title"),
        "rs" : RumahSakit.objects.all().count()
    }
    return render(request, 'homepage.html', context)