from django import forms
from .models import Berita, Comment

class BeritaForm(forms.ModelForm):
    class Meta:
        model = Berita
        fields = "__all__"

class CommentForm(forms.ModelForm):
    class Meta:
        model = Comment
        fields = "__all__"