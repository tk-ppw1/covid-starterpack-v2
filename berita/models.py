from django.db import models
from django.shortcuts import reverse


class Berita(models.Model):
    title = models.TextField(unique=True)
    image_url = models.TextField()
    content = models.TextField()


    def __str__(self):
        return self.title

    def get_absolute_url(self):
        link = self.title.replace(" ","%20")
        return reverse("berita:detailPost",args=[link])

class Comment(models.Model):
    account = models.TextField()
    text = models.TextField()
    post = models.ForeignKey(Berita, on_delete=models.CASCADE)

    def __str__(self):
        return "%s %s" % (self.account, self.post.title)
    

