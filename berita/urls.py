from django.urls import path

from . import views

app_name = 'berita'

urlpatterns = [
    path('', views.pageBerita, name='pageBerita'),
    path('makepost/', views.makePost, name='makePost'),
    path("post/<str:title>",views.detailPost,name="detailPost"),
    path("deletePost/<str:title>",views.deletePost,name="deletePost"),
    path("ajax/postcomment/", views.postComment, name="postComment")

]
