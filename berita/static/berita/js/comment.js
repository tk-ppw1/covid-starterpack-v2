$(document).ready(function(){
    $(".imageBerita").bind("error",function(){
     // Replacing image source
     $(this).attr("src","https://makitweb.com/demo/broken_image/images/noimage.png");
    });

    $.ajax({
        
        type:'POST',
        url:"/berita/ajax/postcomment/",
        data:{      
            title:$('#title').text(),
            action: 'get',
            csrfmiddlewaretoken:$('input[name=csrfmiddlewaretoken]').val(),
        },
        success:function(json){
            var html = "";
            $("#commentList").empty();
            for (i = 0; i < json.length; i++){
                var account = json[i].fields.account;
                var comment = json[i].fields.text; 
                
                html += "<div><u>" + account + "</u></div><div class='bg-secondary' style='font-size: 15px;'>" + comment + "</div>"
            }
            $("#commentList").append(html);
        }
        
    });
   
    $("#commentSubmit").click(function(event) {
       console.log($('#comment').val())
       console.log($('#title').text())
       
       
    $.ajax({
        
        type:'POST',
        url:"/berita/ajax/postcomment/",
        data:{
            account:'tes',
            comment:$('#comment').val(),
            title:$('#title').text(),
            csrfmiddlewaretoken:$('input[name=csrfmiddlewaretoken]').val(),
            action: 'post',
        },
        success:function(json){
            $('#comment').val("");
            
            var html = "";
            $("#commentList").empty();
            for (i = 0; i < json.length; i++){
                var account = json[i].fields.account;
                var comment = json[i].fields.text; 
                
                html += "<div><u>" + account + "</u></div><div class='bg-secondary' style='font-size: 15px;'>" + comment + "</div>"
            }
            $("#commentList").append(html);
        }
        
    });
   });
});