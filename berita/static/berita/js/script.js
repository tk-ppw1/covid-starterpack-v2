$(document).ready(function(){
    $(".imageBerita").bind("error",function(){
     // Replacing image source
     $(this).attr("src","https://makitweb.com/demo/broken_image/images/noimage.png");
    });
    console.log( $(".imageBerita"));

    $(".deletePost").click(function(event) {
        if (confirm('Yakin ingin menghapus berita ini?')) {
            $.ajax({
                type:'POST',
                url: $(this).attr('src'),
                data:{
                    csrfmiddlewaretoken:$('input[name=csrfmiddlewaretoken]').val(),
                },
            })
            location.reload();
        }
    });
    
    $(".deletePost").on({
        mouseenter: function () {
            $(this).css("background-color","red")
        },
        mouseleave: function () {
            $(this).css("background-color","#FFE66D")
        }
    });

});