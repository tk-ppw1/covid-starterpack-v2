from django.test import TestCase, Client
from django.urls import reverse
from .models import Berita
from .views import pageBerita, makePost, deletePost, detailPost, postComment
from django.urls import resolve
from django.contrib.auth.models import User

class TestViews(TestCase):
    def setUp(self):
        self.client = Client()
        self.berita_url = reverse("berita:pageBerita")
        self.makePost_url = reverse("berita:makePost")
        self.berita_test = Berita.objects.create(title = "Test Judul", image_url = "test_url", content = "test isi")
        self.detailPost_url = reverse("berita:detailPost", args=[self.berita_test.title])
        self.deletePost_url = reverse("berita:deletePost", args=[self.berita_test.title])
        self.login2berita_url = "/accounts/login/?next=/berita/"

        self.test_user = User.objects.create_user(username="test_username")
        self.test_user.set_password("testgoodpassword")
        self.test_user.save()
 
    def test_pageberita_page(self):
        self.client.login(username="test_username", password="testgoodpassword")
        
        response = self.client.get(self.berita_url, follow=True)
        self.assertTrue(response.context['user'].is_authenticated)
        self.assertEquals(response.status_code, 200)
        self.assertTemplateUsed(response, "PageBerita_view.html")
        self.assertContains(response, "Berita")
        self.assertContains(response, "Make Post")
        response_function = resolve(self.berita_url)
        self.assertEqual(response_function.func, pageBerita)

    def test_makepost_page(self):
        self.client.login(username="test_username", password="testgoodpassword")
        response = self.client.get(self.makePost_url)
        self.assertEquals(response.status_code, 200)
        self.assertTemplateUsed(response, "MakePost_view.html")
        self.assertContains(response, "Judul")
        self.assertContains(response, "Link Gambar")
        self.assertContains(response, "Teks Berita")
        response_function = resolve(self.makePost_url)
        self.assertEqual(response_function.func, makePost)

    def test_masuk_makepost_page_tapi_belum_login(self):
        response = self.client.get(self.makePost_url, follow=True)
        self.assertEquals(response.status_code, 200)
        self.assertTemplateUsed(response, "registration/login.html")
        self.assertContains(response, "Username")
        self.assertContains(response, "Password")

    def test_detailpost_page_yang_sudah_ada_model(self):
        response = self.client.get(self.detailPost_url)
        self.assertEquals(response.status_code, 200)
        self.assertTemplateUsed(response, "DetailPost_view.html")
        self.assertContains(response, "Test Judul")
        self.assertContains(response, "test isi")
        total = Berita.objects.all().count()
        response_function = resolve(self.detailPost_url)
        self.assertEqual(response_function.func, detailPost)
        self.assertEquals(total, 1)

    def test_detailpost_page_yang_tidak_ada_model(self):
        response = self.client.get(reverse("berita:detailPost", args=["tidak ada title"]))
        self.assertEquals(response.status_code, 404)

    def test_pageberita_ketika_ada_model(self):
        response = self.client.get(self.berita_url)
        self.assertEquals(response.status_code, 200)
        self.assertTemplateUsed(response, "PageBerita_view.html")
        self.assertContains(response, "Test Judul")
        self.assertContains(response, "test_url")
        self.assertContains(response, "test isi")

    def test_add_post_yang_sudah_ada_judul_yang_sama(self):
        self.client.login(username="test_username", password="testgoodpassword")
        response = self.client.post(self.makePost_url, {
            "title" : "Test Judul",
            "image_url" : "https://via.placeholder.com/150x100",
            "content" : "isi berita2"
        })
        self.assertEquals(response.status_code,302)
        total = Berita.objects.all().count()
        self.assertEquals(total, 1)

    def test_add_post_lalu_redirect(self):
        self.client.login(username="test_username", password="testgoodpassword")
        response = self.client.post(self.makePost_url, {
            "title" : "tes judul2",
            "image_url" : "https://via.placeholder.com/150x100",
            "content" : "isi berita2"
        })
        self.assertEquals(response.status_code,302)
        total = Berita.objects.all().count()
        self.assertEquals(total, 2)

    def test_menghapus_post(self):
        response = self.client.post(self.deletePost_url)
        total = Berita.objects.all().count()
        response_function = resolve(self.deletePost_url)
        self.assertEqual(response_function.func, deletePost)
        self.assertEquals(total, 0)

    def test_detailpost_ada_comment(self):
        self.client.login(username="test_username", password="testgoodpassword")

        response = self.client.post("/berita/ajax/postcomment/", {
            "account":"test_username",
            "comment":"tes comment",
            "title":"Test Judul",
            "action":"post",
        }, follow=True)
        self.assertEquals(response.status_code, 200)
        self.assertContains(response, "test_username")
        self.assertContains(response, "tes comment")

    def test_comment_belum_login(self):
        response = self.client.post("/berita/ajax/postcomment/", {
            "account":"test_username",
            "comment":"comment",
            "title":"Test Judul",
            "action":"post",
        }, follow=True)
        self.assertTemplateUsed(response, "PageBerita_view.html")
        self.assertEquals(response.status_code, 200)
        self.assertContains(response, "Berita")

    def test_komen_setelah_reload(self):
        self.client.login(username="test_username", password="testgoodpassword")

        response = self.client.post("/berita/ajax/postcomment/", {
            "title":"Test Judul",
            "action":"get",
        }, follow=True)

        self.assertEquals(response.status_code, 200)
        self.assertNotContains(response, "test_username")
        self.assertNotContains(response, "tes comment")

        response = self.client.post("/berita/ajax/postcomment/", {
            "account":"test_username",
            "comment":"tes comment",
            "title":"Test Judul",
            "action":"post",
        }, follow=True)

        response = self.client.post("/berita/ajax/postcomment/", {
            "title":"Test Judul",
            "action":"get",
        }, follow=True)

        self.assertEquals(response.status_code, 200)
        self.assertContains(response, "test_username")
        self.assertContains(response, "tes comment")

class TestModels(TestCase):
    def setUp(self):
        self.berita_test = Berita.objects.create(title = "Test Judul", image_url = "test_url", content = "test isi")

    def test_model_mahasiswa(self):
        total = Berita.objects.all().count()
        self.assertEquals(total, 1)

    def test_judul_berita(self):
        self.assertEquals(str(self.berita_test),"Test Judul")


