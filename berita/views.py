from django.shortcuts import get_object_or_404, redirect, render
from .forms import BeritaForm, CommentForm
from .models import Berita, Comment
from django.core import serializers
from django.http import HttpResponse, JsonResponse


# Create your views here.
def pageBerita(request):
    context = {
        "listBerita" : Berita.objects.all().order_by("title"),
    }
    return render(request, 'PageBerita_view.html', context)


def makePost(request):
    if not request.user.is_authenticated:
        return redirect('%s?next=%s' % ("/accounts/login/", request.path))
    if request.method == 'POST':
        beritaForm = BeritaForm(request.POST)
        if not beritaForm.data['image_url']:
                mutable = request.POST._mutable
                request.POST._mutable = True
                beritaForm.data['image_url'] = "https://makitweb.com/demo/broken_image/images/noimage.png"
                request.POST._mutable = mutable
        if beritaForm.is_valid():
            beritaForm.save()
        return redirect("berita:pageBerita")
    else:
        return render(request,"MakePost_view.html")

def detailPost(request, title):
    title = title.replace("%20"," ")
    berita = get_object_or_404(Berita,title=title)
    context = {
        "berita" : berita,
    }
    return render(request,"DetailPost_view.html",context)

def deletePost(request, title):
    if request.method == "POST":
        title = title.replace("%20"," ")
        berita = Berita.objects.get(title=title)
        berita.delete()
    return redirect("berita:pageBerita")

def postComment(request):
    if request.user.is_authenticated:
        response_data = {}
        if request.POST.get('action') == 'post':
            title = request.POST.get('title')
            comment = request.POST.get('comment')
            account = request.user.username
            berita = Berita.objects.get(title=title)

            commentModel = Comment(account=account, text=comment, post=berita)
            commentModel.save()
            
            jsonBerita = serializers.serialize('json', berita.comment_set.all())
            return HttpResponse(jsonBerita, content_type="application/json")
        elif request.POST.get('action') == 'get':
            title = request.POST.get('title')
            berita = Berita.objects.get(title=title)
            
            jsonBerita = serializers.serialize('json', berita.comment_set.all())
            return HttpResponse(jsonBerita, content_type="application/json")
    return redirect("berita:pageBerita") 

