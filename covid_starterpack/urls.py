"""covid_starterpack URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import include, path

urlpatterns = [
    path('admin/', admin.site.urls),
    path('', include('homepage.urls')),
    path('covid_kit/', include('covid_kit.urls', namespace='covid_kit')),
    path('daftar-rs/', include(('list_rs.urls', 'list_rs'), namespace='list_rs')),
    path('daftarkasus/', include('list_kasus.urls',namespace ='listkasus')),
    path('berita/', include('berita.urls')),
    path('contact/', include('contact.urls')),
    path('faq/', include('faq.urls', namespace='faq')),
    path('accounts/', include('django.contrib.auth.urls')),
    path('register/', include('register.urls', namespace='register')),
]
