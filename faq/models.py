from django.db import models

# Create your models here.

class ModelPertanyaan(models.Model):
    pertanyaan = models.CharField(max_length=1000)

    def __str__(self):
        return self.pertanyaan


class ModelJawaban(models.Model):
    pengisi = models.CharField(max_length=100)
    jawaban = models.TextField(blank=False)
    isi_pertanyaan = models.OneToOneField(ModelPertanyaan, on_delete=models.CASCADE, primary_key=True)

class ModelInfo(models.Model):
    topic = models.CharField(max_length=1000)
    pemberi = models.CharField(max_length=1000)
    info = models.TextField(blank=False)

    def __str__(self):
        return self.topic
