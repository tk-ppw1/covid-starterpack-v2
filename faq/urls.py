from django.urls import path, include

from . import views

app_name = 'faq'

urlpatterns = [
    path('', views.faq, name='faq'),
    path('info/', views.informasi, name='info'),
    path('list-pertanyaan/', views.list_pertanyaan, name='list-pertanyaan'),
    path('jawab/P<int:id_pertanyaan>/', views.jawab, name='jawab'),
    path('list-FAQ/', views.list_faq, name='list-faq'),
    path('detail/P<int:id_pertanyaan>/', views.detail, name='detail'),
]
