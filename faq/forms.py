from django import forms

class FormPertanyaan(forms.Form):
    pertanyaan = forms.CharField(
        widget = forms.TextInput(attrs={'placeholder':'Ask your question here...', 'style':'width:30%; border-radius:5px; border: none'})
    )

class FormJawaban(forms.Form):
    pengisi = forms.CharField(
        label = 'Dijawab oleh:',
        widget = forms.TextInput(attrs={'style':'width:100%'})
    )
    jawaban = forms.CharField(
        widget = forms.Textarea(attrs={'style':'width:100%'})
    )

class FormInfo(forms.Form):
    topic = forms.CharField(
        label = 'Topic:',
        widget = forms.TextInput(attrs={'placeholder':'Topic', 'style':'width:100%; border-radius: 5px; width: 350px; padding-right: 5px; border: none', 'id':'topic'})
    )
    pemberi = forms.CharField(
        label = 'Name:',
        widget = forms.TextInput(attrs={'placeholder':'Name', 'style':'width:100%; border-radius: 5px; width: 350px; padding-right: 5px; border: none', 'id':'pemberi'})
    )
    info = forms.CharField(
        label= 'Information:',
        widget = forms.Textarea(attrs={'placeholder':'Let us know what you know or your experience', 'style':'width:100%; border-radius: 5px; width: 350px; padding-right: 5px; border: none', 'id':'info'})
    )