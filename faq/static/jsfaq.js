$( function() {
    $( "#tentang-satu" ).accordion({
        collapsible: true,
        active: false,
        heightStyle: "panel",
    });
    $( "#tentang-dua" ).accordion({
        collapsible: true,
        active: false,
        heightStyle: "panel",
    });
    $( "#tentang-tiga" ).accordion({
        collapsible: true,
        active: false,
        heightStyle: "panel",
    });
    $( "#cegah-satu" ).accordion({
        collapsible: true,
        active: false,
        heightStyle: "panel",
    });
    $( "#cegah-dua" ).accordion({
        collapsible: true,
        active: false,
        heightStyle: "panel",
    });
    $( "#cegah-tiga" ).accordion({
        collapsible: true,
        active: false,
        heightStyle: "panel",
    });
    $( "#penanganan-satu" ).accordion({
        collapsible: true,
        active: false,
        heightStyle: "panel",
    });
    $( "#penanganan-dua" ).accordion({
        collapsible: true,
        active: false,
        heightStyle: "panel",
    });
    $( "#penanganan-tiga" ).accordion({
        collapsible: true,
        active: false,
        heightStyle: "panel",
    });
} );

$(document).ready(function() {
    $(".heart.fa").click(function() {
        $(this).toggleClass("fa-heart fa-heart-o");
    });
});

$(document).on('submit', '#post-info',function(e){
    e.preventDefault();
    e.stopImmediatePropagation();
    $.ajax({
        type:'POST',
        url: '/faq/info/',
        data:{
            topic:$('#topic').val(),
            pemberi:$('#pemberi').val(),
            info:$('#info').val(),
            csrfmiddlewaretoken:$('input[name=csrfmiddlewaretoken]').val(),
            action: 'post'
        },
        success:function(json){
            document.getElementById("post-info").reset();

            $(".fr-form.col.d-flex.flex-column").prepend('<div class="justify-content-start align-items-left mt-2">' +
                '<b style="color: black">Topic: ' + json.topic + '</b><br>'+
                '<small style="color: black">Nama: ' + json.pemberi + '</small>'+
            '</div>' +
            '<div class="justify-content-start align-items-left">'+
                '<p style="color: black; padding-left: 0px;">Information: ' + json.info + '</p>'+
            '</div>'
            )
            console.log(json);
        },
        error : function(xhr,errmsg,err) {
        console.log(xhr.status + ": " + xhr.responseText);
        }
    });
});