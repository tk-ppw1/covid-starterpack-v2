from django.shortcuts import render, redirect
from django.http import HttpResponseRedirect, JsonResponse

# Create your views here.
from .forms import FormPertanyaan, FormJawaban, FormInfo
from .models import ModelPertanyaan, ModelJawaban, ModelInfo

def faq(request):
    form_pertanyaan = FormPertanyaan()
    if request.method == 'POST':
        input_form_pertanyaan = FormPertanyaan(request.POST)
        if input_form_pertanyaan.is_valid():
            ModelPertanyaan.objects.create(
                pertanyaan = input_form_pertanyaan.cleaned_data.get('pertanyaan')
            )
        return HttpResponseRedirect('/faq/list-pertanyaan/')    
    context = {
        'form_pertanyaan':form_pertanyaan
    }
    return render(request, 'faq/faq.html', context)

def list_pertanyaan(request):
    pertanyaans = ModelPertanyaan.objects.all()
    context = {
        'pertanyaans':pertanyaans
    }
    return render(request, 'faq/list-pertanyaan.html', context)

def jawab(request, id_pertanyaan):
    form_jawaban = FormJawaban()
    if request.method == 'POST':
        input_form_jawaban = FormJawaban(request.POST)
        if input_form_jawaban.is_valid():
            data = input_form_jawaban.cleaned_data
            objek_jawaban = ModelJawaban()
            objek_jawaban.pengisi = data['pengisi']
            objek_jawaban.jawaban = data['jawaban']
            objek_jawaban.isi_pertanyaan = ModelPertanyaan.objects.get(id=id_pertanyaan)
            objek_jawaban.save()
            return HttpResponseRedirect('/faq/list-FAQ/')
    context = {
        'form_jawaban':form_jawaban
    }
    return render(request, 'faq/jawab.html', context)

def list_faq(request):
    faqs = ModelPertanyaan.objects.all()
    context = {
        'faqs':faqs
    }
    return render(request, 'faq/list-faq.html', context)

def detail(request, id_pertanyaan):
    pertanyaan = ModelPertanyaan.objects.filter(id=id_pertanyaan).get(id=id_pertanyaan)
    jawabans = ModelJawaban.objects.all()
    context = {
        'pertanyaan':pertanyaan,
        'jawabans':jawabans
    }
    return render(request, 'faq/detail.html', context)

def informasi(request):
    form_info = FormInfo()
    infos = ModelInfo.objects.all().order_by('-id')
    response_data = {}

    if request.method == 'POST':
        # print("oke")
        input_form_info = FormInfo(request.POST)
        if input_form_info.is_valid():
            data = input_form_info.cleaned_data
            objek_info = ModelInfo()
            objek_info_topic = data['topic']
            objek_info_pemberi = data['pemberi']
            objek_info_info = data['info']

            ModelInfo.objects.create(
                topic = request.POST.get('topic'),
                pemberi = request.POST.get('pemberi'),
                info = request.POST.get('info')
            )

        response_data['topic'] = objek_info_topic
        response_data['pemberi'] = objek_info_pemberi
        response_data['info'] = objek_info_info
        return JsonResponse(response_data)
    context = {
        'infos':infos,
        'form_info':form_info
    }

    return render(request, 'faq/info.html', context)  
