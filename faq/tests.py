from django.test import TestCase, Client
from .models import ModelJawaban, ModelPertanyaan, ModelInfo
from .forms import FormJawaban, FormPertanyaan, FormInfo
from .views import faq, list_pertanyaan, list_faq, jawab, detail, informasi
from django.urls import resolve, reverse
from .apps import FaqConfig
from django.apps import apps
from django.http import HttpRequest
import json
from django.contrib.auth.models import User

# Create your tests here.
class TestUrls(TestCase):
    def test_apakah_url_faq_ada(self):
        response = Client().get('/faq/')
        self.assertEqual(response.status_code, 200)

    def test_apakah_url_list_pertanyaan_ada(self):
        response = Client().get('/faq/list-pertanyaan/')
        self.assertEqual(response.status_code, 200)

    def test_apakah_url_list_faq_ada(self):
        response = Client().get('/faq/list-FAQ/')
        self.assertEqual(response.status_code, 200)

    def test_apakah_url_jawab_ada(self):
        response = Client().get('/faq/jawab/P8/')
        self.assertEqual(response.status_code, 200)

class TestViews(TestCase):
    def test_views_faq(self):
        found = resolve('/faq/')
        self.assertEqual(found.func, faq)

    def test_views_list_pertanyaan(self):
        found = resolve('/faq/list-pertanyaan/')
        self.assertEqual(found.func, list_pertanyaan)

    def test_views_jawab(self):
        found = resolve('/faq/jawab/P8/')
        self.assertEqual(found.func, jawab)

    def test_views_list_faq(self):
        found = resolve('/faq/list-FAQ/')
        self.assertEqual(found.func, list_faq)

    def test_views_detail(self):
        found = resolve('/faq/detail/P8/')
        self.assertEqual(found.func, detail)
    
    def test_redirect_url_setelah_isi_pertanyaan(self):
        response = Client().get('/faq/list-pertanyaan')
        self.assertEqual(response.status_code, 301)
    
    def test_redirect_url_setelah_isi_jawaban(self):
        response = Client().get('/faq/list-FAQ')
        self.assertEqual(response.status_code, 301)

class TestHTML(TestCase):
    def test_faq_templates_is_used(self):
        response = Client().get('/faq/')
        self.assertTemplateUsed(response, 'faq/faq.html')

    def test_list_pertanyaan_templates_is_used(self):
        response = Client().get('/faq/list-pertanyaan/')
        self.assertTemplateUsed(response, 'faq/list-pertanyaan.html')
    
    def test_list_faq_templates_is_used(self):
        response = Client().get('/faq/list-FAQ/')
        self.assertTemplateUsed(response, 'faq/list-faq.html')
    
    def test_jawab_templates_is_used(self):
        response = Client().get('/faq/jawab/P8/')
        self.assertTemplateUsed(response, 'faq/jawab.html')

class TestApp(TestCase):
    def test_apps(self):
        self.assertEqual(FaqConfig.name, 'faq')
        self.assertEqual(apps.get_app_config('faq').name, 'faq')

class TestModel(TestCase):
    def setUp(self):
        objek_jawaban = ModelJawaban(jawaban="Wajib")
        objek_jawaban.save()

        self.pertanyaan = ModelPertanyaan.objects.create(pertanyaan="Apakah menggunakan masker wajib?")
        self.topic = ModelInfo.objects.create(topic="Masker")

    def test_instance_created(self):
        self.assertEqual(ModelJawaban.objects.all().count(), 1)
        self.assertEqual(ModelPertanyaan.objects.all().count(), 1)
        self.assertEqual(ModelInfo.objects.all().count(), 1)
        self.assertEqual(str(self.pertanyaan), "Apakah menggunakan masker wajib?")
        self.assertEqual(str(self.topic), "Masker")
    
    def test_get_model_by_id(self):
        model_tanya = ModelPertanyaan(pertanyaan="apa?")
        model_tanya.save()
        model_jawab = ModelJawaban(pengisi="aku", jawaban="ya",
            isi_pertanyaan=ModelPertanyaan.objects.get(id=1))
        model_jawab.save()
        response = self.client.get(reverse('faq:detail', args=[model_tanya.id]))
        self.assertEqual(response.status_code, 200)

class TestForm(TestCase):
    def setUp(self):
        model_tanya = ModelPertanyaan(pertanyaan="apa?")
        model_tanya.save()
        model_jawab = ModelJawaban(pengisi='aku', jawaban='ya', isi_pertanyaan=ModelPertanyaan.objects.get(id=1))
        model_jawab.save()
        model_info = ModelInfo(topic='ok', pemberi='aku', info='nice')
        model_info.save()
        self.faq = reverse('faq:faq')
        self.jawab = reverse('faq:jawab', args=[model_tanya.id])
        self.informasi = reverse('faq:info')

    def test_post_form_tanya(self):
        response = Client().post('/faq/list-pertanyaan/', data={'pertanyaan':'apa?'})
        self.assertEquals(response.status_code, 200)

    def test_post_form_jawab(self):
        response = Client().post('/faq/list-FAQ/', data={'pengisi':'aku', 'jawaban':'ya'})
        self.assertEquals(response.status_code, 200)

    def test_form_info_redirect(self):
        response = Client().get(reverse('faq:info'))
        self.assertEqual(response.status_code, 200)

    def test_form_bikin_pertanyaan(self):
        response = Client().post(self.faq, data={
            'pertanyaan':'apa?'
        })

        response = Client().post(self.jawab, data={
            'pengisi':'aku',
            'jawaban':'ya'
        })

        response = Client().post(self.informasi, data={
            'topic':'wow',
            'pemberi':'ok',
            'info':'nice info'
        })
        self.assertEqual(response.status_code, 200)

    def test_form_is_valid(self):
        form_tanya = FormPertanyaan(data={
            'pertanyaan':'apa?'
        })
        self.assertTrue(form_tanya.is_valid())

        form_jawab = FormJawaban(data={
            'pengisi':'aku',
            'jawaban':'apa',
        })
        self.assertTrue(form_jawab.is_valid())

        form_info = FormInfo(data={
            'topic':'masker',
            'pemberi':'aku',
            'info':'nice info',
        })
        self.assertTrue(form_info.is_valid())

    def test_form_invalid(self):
        form_tanya = FormPertanyaan(data={})
        self.assertFalse(form_tanya.is_valid())

        form_jawab = FormJawaban(data={})
        self.assertFalse(form_jawab.is_valid())

        form_info = FormInfo(data={})
        self.assertFalse(form_info.is_valid())


class TestLogin(TestCase):
    def setUp(self):
        self.client = Client()
        self.user_baru = User.objects.create_user(username='aran')
        self.user_baru.set_password('doun1234')
        self.user_baru.save()

    def test_belum_login(self):
        response = self.client.get('/faq/')
        self.assertFalse(response.context['user'].is_authenticated)
    
    def test_udah_login(self):
        self.client.login(username='aran', password='doun1234')
        response = self.client.get('/faq/')
        self.assertTrue(response.context['user'].is_authenticated)
        html_response = response.content.decode('utf8')
        self.assertIn('Send', html_response)