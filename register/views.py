from django.contrib.auth import login, authenticate
from django.contrib.auth import logout as django_logout
from django.contrib.auth.forms import UserCreationForm
from django.shortcuts import render, redirect
from .forms import RegisterForm


def register(request):
    if request.method == 'POST':
        form = RegisterForm(request.POST)
        if form.is_valid():
            form.save()
            username = form.cleaned_data.get('username')
            password = form.cleaned_data.get('password1')
            user = authenticate(username=username, password=password)
            login(request, user)
            return redirect('homepage:homepage')
    else:
        form = RegisterForm()
    return render(request, 'register/register_views.html', {'form': form})

# def log_in(request):
#     form = UserLoginForm(request.POST or None)
#     context = {
#         'form': form
#     }
#     print(request.user.is_authenticated)
#     if form.is_valid():
#         print(form.cleaned_data)
#         username = form.cleaned_data.get('username')
#         password = form.cleaned_data.get('password')
#         user = authenticate(request, username=username, password=password)
#         if user is not None:
#             login(request, user)
#             return redirect('')
#         else:
#             print("error.......")

#     return render(request, "registration/login.html", context=context)

def profile(request):
    return render(request, 'profile/profile.html')
