from django import forms
from django.contrib.auth.forms import UserCreationForm, UserChangeForm, PasswordChangeForm
from django.contrib.auth.models import User


class RegisterForm(UserCreationForm):
    first_name = forms.CharField(max_length=30, required=True, initial='Anon', widget=forms.TextInput(attrs={'style':'width:100%; border-radius:5px; border:none'}))
    last_name = forms.CharField(max_length=30, required=False, widget=forms.TextInput(attrs={'style':'width:100%; border-radius:5px; border:none'}))
    email = forms.EmailField(max_length=254, widget=forms.TextInput(attrs={'style':'width:100%; border-radius:5px; border:none'}))
    username = forms.CharField(widget=forms.TextInput(attrs={'style':'width:100%; border-radius:5px; border:none'}))
    password1 = forms.CharField(label = "Password", widget=forms.PasswordInput(attrs={'style':'width:100%; border-radius:5px; border:none'}))
    password2 = forms.CharField(label = "Password confirmation", widget=forms.PasswordInput(attrs={'style':'width:100%; border-radius:5px; border:none'}))

    class Meta(UserCreationForm.Meta):
        model = User
        fields = ('username', 'first_name', 'last_name', 'email', 'password1', 'password2', )

# class UserLoginForm(forms.Form):
#     username = forms.CharField(widget=forms.TextInput(
#         attrs={'style':'width:100%; border-radius:5px; border:none'}))
#     password = forms.CharField(widget=forms.PasswordInput(
#         attrs={'style':'width:100%; border-radius:5px; border:none'}))

class EditProfileForm(UserChangeForm):
    username = forms.CharField(label="Username", widget=forms.TextInput(attrs={'class': 'form-control'}), help_text='Hanya bisa huruf, angka, dan @/./+/-/_ saja')
    first_name = forms.CharField(label="First Name", widget=forms.TextInput(attrs={'class': 'form-control'}))
    last_name = forms.CharField(label="Last Name", widget=forms.TextInput(attrs={'class': 'form-control'}))
    email = forms.EmailField(label="Email Address", widget=forms.TextInput(attrs={'class': 'form-control'}))
    password = None
    class Meta:
        model = User
        fields = (
            'username',
            'first_name',
            'last_name',
            'email',
        )

class ChangePassForm(PasswordChangeForm):
    old_password = forms.CharField(label = "Password Lama", widget=forms.PasswordInput(attrs={'class': 'form-control'}))
    new_password1 = forms.CharField(label= "Password Baru", widget=forms.PasswordInput(attrs={'class': 'form-control'}), help_text='Minimal 8 huruf dan tidak bisa semuanya angka.')
    new_password2 = forms.CharField(label = "Konfirmasi Password Baru", widget=forms.PasswordInput(attrs={'class': 'form-control'}), help_text='Masukkan password pada kolom sebelumnya untuk verifikasi.')

    class Meta(UserCreationForm.Meta):
        model = User
        fields = ['old_password', 'new_password1', 'new_password2']