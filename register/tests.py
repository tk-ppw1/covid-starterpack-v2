from django.test import TestCase, Client
from django.urls import reverse, resolve
from .views import register
from django.contrib.auth.models import User


class TestViews(TestCase):
    def setUp(self):
        self.client = Client()
        self.signup_url = reverse("register:register")
        self.login_url = "/accounts/login/?next=/"
        self.logout_url = "/accounts/logout/?next=/"
        self.test_user = User.objects.create_user("test_username", password="testgoodpassword")
        self.test_user.save()

    
    def test_login_page(self):
        response = self.client.get(self.login_url)
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, "registration/login.html")
        self.assertContains(response, "Username")
        self.assertContains(response, "Password")
        self.assertContains(response, "Log In")
        
    def test_signup_page(self):
        response = self.client.get(self.signup_url)
        self.assertEquals(response.status_code, 200)
        self.assertTemplateUsed(response, "register/register_views.html")
        self.assertContains(response, "Sign Up")
        self.assertContains(response, "Username")
        self.assertContains(response, "Password")
        response_function = resolve(self.signup_url)
        self.assertEqual(response_function.func, register)  

    def test_signup_form_dan_menampilkan_nama_di_homepage(self):
        response = self.client.post(self.signup_url , {
            'username': "tes",
            'email': "www.tes@gmail.com",
            'first_name': "nama",
            'last_name': "ini juga",
            'password1': "passwordbagus",
            'password2': "passwordbagus"
        }, follow=True)
        self.assertEqual(response.status_code, 200)
        self.assertTrue(response.context['user'].is_authenticated)
        self.assertTemplateUsed(response, "homepage.html")
        self.assertContains(response, "Penjelasan COVID")

    def test_signup_form_password_berbeda(self):
        response = self.client.post(self.signup_url , {
            'username': "tes",
            'email': "tes@gmail.com",
            'first_name': "nama",
            'last_name': "ini juga",
            'password1': "passwordbagus",
            'password2': "passwordbagas"
        }, follow=True)
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, "register/register_views.html")
        self.assertContains(response, "The two password fields didn’t match")

    def test_signup_form_email_tidak_valid(self):
        response = self.client.post(self.signup_url , {
            'username': "tes",
            'email': "a@f",
            'first_name': "nama",
            'last_name': "ini juga",
            'password1': "passwordbagus",
            'password2': "passwordbagus"
        })
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, "register/register_views.html")
        self.assertContains(response, "Enter a valid email address")

    def test_signup_form_username_tidak_valid(self):
        response = self.client.post(self.signup_url , {
            'username': "username tidak valid",
            'email': "tes@gmail.com",
            'first_name': "nama",
            'last_name': "ini juga",
            'password1': "passwordbagus",
            'password2': "passwordbagus"
        })
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, "register/register_views.html")
        self.assertContains(response, "Enter a valid username. This value may contain only letters, numbers, and @/./+/-/_ characters")

    def test_signup_form_password_mirip_username(self):
        response = self.client.post(self.signup_url , {
            'username': "qwerty",
            'email': "tes@gmail.com",
            'first_name': "nama",
            'last_name': "ini juga",
            'password1': "qwerty",
            'password2': "qwerty"
        })
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, "register/register_views.html")
        self.assertContains(response, "The password is too similar to the username")

    def test_signup_form_password_terlalu_umum(self):
        response = self.client.post(self.signup_url , {
            'username': "test",
            'email': "tes@gmail.com",
            'first_name': "nama",
            'last_name': "ini juga",
            'password1': "qwerty",
            'password2': "qwerty"
        })
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, "register/register_views.html")
        self.assertContains(response, "This password is too common")

    def test_login_password_salah_atau_tidak_ada_username_terdaftar(self):
        response = self.client.post(self.login_url , {
            'username': "test",
            'password1': "qwerty",
        })
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, "registration/login.html")

    def test_login_berhasil(self):
        response = self.client.post(self.login_url, {
            "username" : "test_username",
            "password" : "testgoodpassword"
        }, follow = True)
        self.assertEqual(response.status_code, 200)
        self.assertTrue(response.context['user'].is_authenticated)
        self.assertTemplateUsed(response, "homepage.html")
        self.assertContains(response, "Penjelasan COVID")
        self.assertContains(response, "Halo, tes")

    def test_signup_logout_login(self):
        response = self.client.post(self.signup_url , {
            'username': "tes",
            'email': "www.tes@gmail.com",
            'first_name': "nama",
            'last_name': "ini juga",
            'password1': "passwordbagus",
            'password2': "passwordbagus"
        }, follow=True)
        self.assertEqual(response.status_code, 200)
        self.assertTrue(response.context['user'].is_authenticated)
        self.assertTemplateUsed(response, "homepage.html")
        self.assertContains(response, "Penjelasan COVID")
        self.assertContains(response, "Halo, tes")

        response = self.client.post(self.logout_url, follow=True)
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, "homepage.html")
        self.assertContains(response, "Login")
        self.assertContains(response, "Register")
        self.assertFalse(response.context['user'].is_authenticated)
        

        response = self.client.post(self.login_url, {
            "username" : "tes",
            "password" : "passwordbagus"
        }, follow = True)
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, "homepage.html")
        self.assertContains(response, "Halo, tes")
        self.assertTrue(response.context['user'].is_authenticated)

    def test_profile(self):
        response = self.client.get(reverse("register:profile"))
        self.assertFalse(response.context['user'].is_authenticated)
        self.assertEquals(response.status_code, 200)
        html_kembalian = response.content.decode('utf8')
        self.assertNotIn("Username:", html_kembalian)
        self.assertIn("Login untuk melihat profil akun Anda.", html_kembalian)
    
    def test_profile_setelah_login(self):
        self.client.login(username="test_username", password="testgoodpassword")
        response = self.client.get(reverse("register:profile"))
        self.assertTrue(response.context['user'].is_authenticated)
        self.assertEquals(response.status_code, 200)
        html_kembalian = response.content.decode('utf8')
        self.assertIn("Username:", html_kembalian)
        self.assertIn("test_username", html_kembalian)
    







        


