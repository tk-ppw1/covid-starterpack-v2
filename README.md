[![pipeline status](https://gitlab.com/tk-ppw1/covid-starterpack-v2/badges/master/pipeline.svg)](https://gitlab.com/tk-ppw1/covid-starterpack-v2/-/commits/master)
[![coverage report](https://gitlab.com/tk-ppw1/covid-starterpack-v2/badges/master/coverage.svg)](https://gitlab.com/tk-ppw1/covid-starterpack-v2/-/commits/master)

Anggota Kelompok
1. Aransandya Pangesthika Arrumy
2. Evita Haliansyah
3. Hendrico Kristiawan
4. Rizky Musyaffa Fajari
5. Tsanaativa Vinnera

Link Herokuapp
https://covid-starterpack.herokuapp.com/

Cerita Aplikasi
covid-starterpack adalah sebuah aplikasi yang bertujuan untuk memudahkan masyarakat Indonesia dalam menghadapi pandemi COVID-19 yang tengah terjadi saat ini. Ada pun kegunaan aplikasi ini antara lain adalah untuk mengetahui berita terupdate seputar COVID-19, angka COVID-19 terbaru, dan data rumah sakit rujukan yang menangani COVID-19. Selain itu aplikasi ini juga menyediakan jawaban dari pertanyaan yang sering ditanyakan selama pandemi dan juga menjual barang-barang yang dibutuhkan selama masa pandemi ini.

Daftar Fitur
1. FAQ
Pada fitur FAQ, pengunjung website bisa melihat jawaban dari pertanyaan-pertanyaan yang umum ditanyakan dan mencari pertanyaan pada search bar berdasarkan keyword yang mereka ingin cari.
2. Jumlah Kasus COVID-19
Pada Fitur ini pengunjung website dapat melihat jumlah orang yang teridentifikasi, jumlah orang yang sembuh,dan jumlah orang yang meninggal di Indonesia dan pengunjung juga dapat mencari jumlah kasus di daerah/Provinsi yang mereka mau
3. Daftar rumah sakit rujukan
Pada fitur ini tersedia daftar rumah sakit rujukan COVID-19. Pengguna website dapat menambahkan, menghapus maupun meng-update data pada daftar tersebut. Mereka juga dapat mendaftar antrian untuk melakukan rapid test dan swab test pada suatu rumah sakit.
4. Covid Kit
Covid Kit adalah fitur covid-starterpack yang menyediakan berbagai macam barang yang dibutuhkan selama pandemi dengan harga yang terjangkau.
5. Berita
Fitur ini menyediakan berita mengenai COVID yang terjadi di Indonesia. Selain itu pengguna juga boleh untuk menceritakan bagaimana kondisi di daerah mereka maupun kondisi mereka selama pandemi. 