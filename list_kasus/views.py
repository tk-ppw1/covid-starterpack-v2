from django.shortcuts import render, redirect
from django.http import HttpResponse, JsonResponse
import requests
import json
from .models import ListKasus,KasusIndo
from . import models
from .forms import formKasus
from operator import itemgetter

# Create your views here.

def list_kasus(request):
    list_kasus = ListKasus.objects.all()
    
    kasus = KasusIndo.objects.all()

    a = []
    a.append(kasus)
    if all(x for x in a) == False:
        models.KasusIndo.objects.create(nama='Indonesia',jumlah_total='463007',kasus_sembuh='388094',kasus_meninggal='15148')
    else : 
        pass
    kasus = KasusIndo.objects.all()
    context = {'list_kasus':list_kasus,'kasus':kasus}
    return render(request,'list_kasus/listkasus.html',context)


def kasus_detail(request, pk):
    kasus = ListKasus.objects.all().filter(id=pk)
    list_kasus = ListKasus.objects.all()
    context = {'kasus': kasus,'list_kasus':list_kasus}
    return render(request,'list_kasus/kasusdetail.html',context)

def tambah_kasus(request):
    if not request.user.is_authenticated:
        return redirect('%s?next=%s' % ("/accounts/login/", request.path))
    form = formKasus()
    response_data = {}
    if(request.method == 'POST'):
        form = formKasus(request.POST)
        if(form.is_valid()):
            data = form.cleaned_data
            lisstkasus = ListKasus()
            nama_daerah =  data['nama_daerah']
            jumlah_total = data['jumlah_total']
            kasus_sembuh = data['kasus_sembuh']
            kasus_meninggal = data['kasus_meninggal']
            ListKasus.objects.create(
                nama_daerah = request.POST.get('nama_daerah'),
                jumlah_total = request.POST.get('jumlah_total'),
                kasus_sembuh = request.POST.get('kasus_sembuh'),
                kasus_meninggal = request.POST.get('kasus_meninggal'),
            )
            response_data['nama_daerah'] = nama_daerah
            response_data['jumlah_total'] = jumlah_total
            response_data['kasus_sembuh'] = kasus_sembuh
            response_data['kasus_meninggal'] = kasus_meninggal

            return JsonResponse(response_data)
    
    context = {'form':form}
    return render(request,'list_kasus/tambahkasus.html',context)

def kasus_provinsi(request):
    return render(request,'list_kasus/kasusprovinsi.html')

def kasus_input(request):
    return render(request,'list_kasus/kasusinputan.html')

def api_kasus_provinsi(request):
    url = "https://indonesia-covid-19.mathdro.id/api/provinsi"
    ret = requests.get(url)
    data = json.loads(ret.content)
    return JsonResponse(data, safe=False)

def api_kasus_Indo(request):
    url = "https://apicovid19indonesia-v2.vercel.app/api/indonesia"
    ret = requests.get(url)
    data = json.loads(ret.content)
    return JsonResponse(data, safe=False)

def daftar_input(request):
    kasuz = ListKasus.objects.all()
    result = []
    for i in kasuz:
        temp = dict()
        temp['nama_daerah'] = i.nama_daerah
        temp['jumlah_total'] = i.jumlah_total
        temp['kasus_sembuh'] = i.kasus_sembuh
        temp['kasus_meninggal'] = i.kasus_meninggal
        temp['tanggal'] = i.tanggal
        temp['pk'] = i.pk
        result.append(temp)
    context = {
        'result': result,
    }
    return JsonResponse(context)




