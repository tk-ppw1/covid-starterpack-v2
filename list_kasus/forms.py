from django import forms
from .models import ListKasus

class formKasus(forms.ModelForm):
    class Meta:
        model = ListKasus
        fields = "__all__"

        widgets = {
            'nama_daerah' : forms.TextInput(attrs={'required': True,'id':'namee'}),
            'jumlah_total' : forms.NumberInput(attrs={'required': True,'id':'jumlahtotal'}),
            'kasus_sembuh' : forms.NumberInput(attrs={'required': True,'id':'jumlahsembuh'}),
            'kasus_meninggal' : forms.NumberInput(attrs={'required': True,'id':'jumlahmeninggal'}),
        }