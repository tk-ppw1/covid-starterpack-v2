from django.db import models

# Create your models here.
class ListKasus(models.Model):
    nama_daerah = models.CharField(max_length=50)
    jumlah_total = models.IntegerField()
    kasus_sembuh = models.IntegerField()
    kasus_meninggal = models.IntegerField()
    tanggal = models.DateTimeField(auto_now_add = True)

    def __str__(self):
        return self.nama_daerah

class KasusIndo(models.Model):
    nama = models.CharField(max_length=50)
    jumlah_total = models.IntegerField()
    kasus_sembuh = models.IntegerField()
    kasus_meninggal = models.IntegerField()
    tanggal = models.DateTimeField(auto_now= True)

    def __str__(self):
        return self.nama


