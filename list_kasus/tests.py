from django.test import TestCase, Client,RequestFactory
from django.urls import reverse, resolve
from . import models
from . import views
from . import apps
from django.contrib.auth.models import User
# Create your tests here.

class Tests(TestCase): 
    def setUp(self):
        self.client = Client()
        self.kasus = models.ListKasus.objects.create(nama_daerah='a',jumlah_total='5',kasus_sembuh='5',kasus_meninggal='0')
        self.tambahurl = reverse("listkasus:tambahkasus")
        self.test_user = User.objects.create_user(username="zorbax")
        self.test_user.set_password("dimanapun321")
        self.test_user.save()
        

    def test_kasus_url_i(self):
        response = Client().get('/daftarkasus/')
        self.model_pk = models.KasusIndo.objects.get(pk=1)
        aww = models.KasusIndo.objects.filter(nama="Indonesia").count()
        self.assertEqual(aww,1)
        self.assertEqual(str(self.model_pk), "Indonesia")


    def test_str(self):
        self.assertEqual(str(self.kasus), "a")
        

    def test_kasus_url_is_resolved(self):
        self.client.login(username="zorbax", password="dimanapun321")
        response = self.client.get('/daftarkasus/')
        self.assertEquals(response.status_code, 200)

    def test_kasus_url_is_resolved2(self):
        self.client.login(username="zorbax", password="dimanapun321")
        response = self.client.get(self.tambahurl,follow=True)
        self.assertEquals(response.status_code, 200)

    def test_kasus_url_provinsi_is_resolved(self):
        response = self.client.get('/daftarkasus/kasusprovinsi/')
        self.assertEquals(response.status_code, 200)

    def test_kasus_url_data_api_is_resolved(self):
        response = self.client.get('/daftarkasus/data/')
        self.assertEquals(response.status_code, 200)

    def test_url_input_is_resolved(self):
        response = self.client.get('/daftarkasus/datainput/')
        self.assertEquals(response.status_code, 200)

    def test_url_kasus_input_is_resolved(self):
        response = self.client.get('/daftarkasus/kasusinput/')
        self.assertEquals(response.status_code, 200)

    def test_kasus_url_data_api2_is_resolved(self):
        response = self.client.get('/daftarkasus/dataIndonesia/')
        self.assertEquals(response.status_code, 200)

    def test_kasus_url_sebelum_login(self):
        response = self.client.get(self.tambahurl,follow=True)
        self.assertEquals(response.status_code, 200)

    def test_kasus_url_is_resolved3(self):
        response = Client().get('/daftarkasus/kasus_detail/1/')
        self.assertEquals(response.status_code, 200)

    def test_form_save_a_POST_request(self):
        self.client.login(username="zorbax", password="dimanapun321")
        response = self.client.post(self.tambahurl, data={'nama_daerah':'z','jumlah_total':'5','kasus_sembuh':'5','kasus_meninggal':'0'})
        jumlah = models.ListKasus.objects.filter(nama_daerah='z').count()
        self.assertEqual(jumlah,1)


    def testConfig(self):
        self.assertEqual(apps.ListKasusConfig.name, 'list_kasus')
