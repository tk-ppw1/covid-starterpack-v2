from django.urls import path
from . import views
app_name = 'list_kasus'

urlpatterns = [

    path('',views.list_kasus,name = 'list_kasus'),
    path('kasus_detail/<int:pk>/',views.kasus_detail, name = 'kasusdetail'),
    path('tambahkasus/', views.tambah_kasus, name='tambahkasus'),
    path('kasusprovinsi/', views.kasus_provinsi, name='kasusprovinsi'),
    path('kasusinput/', views.kasus_input, name='kasusinput'),
    path('datainput/', views.daftar_input, name = 'datainput'),
    path('data/',views.api_kasus_provinsi,name='data'),
    path('dataIndonesia/',views.api_kasus_Indo,name='dataIndonesia'),
]